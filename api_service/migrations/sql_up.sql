create table if not exists auth_users
(
    user_id    serial
    constraint auth_users_pk_user_id
    primary key,
    username   text      not null
    constraint auth_users_uniq_username
    unique,
    email      text      not null
    constraint auth_users_pk_uniq_email
    unique,
    created_at timestamp not null,
    updated_at timestamp not null,
    deleted_at timestamp,
    password   text      not null
);

create table if not exists auth_tokens
(
    user_id       integer not null
    constraint auth_tokens_uniq_user_id
    unique
    constraint auth_tokens_auth_users_null_fk
    references auth_users
    on update cascade on delete cascade,
    access_token  text    not null
    constraint auth_tokens_pk_uniq_access_token
    unique,
    refresh_token text    not null
    constraint auth_tokens_pk_uniq_refresh_token
    unique
);

create table if not exists permissions
(
    perm_id    serial
    constraint permissions_pk
    primary key,
    name       text      not null
    constraint permissions_pk_uuniq_name
    unique,
    created_at timestamp not null,
    updated_at timestamp not null,
    deleted_at timestamp
);

create table if not exists groups
(
    group_id   serial
    constraint groups_pk
    primary key,
    name       text      not null
    constraint groups_uniq_name
    unique,
    created_at timestamp not null,
    updated_at timestamp not null,
    deleted_at timestamp
);

create table if not exists group_permissions
(
    group_id serial
    constraint group_permissions_groups_null_fk
    references groups,
    perm_id  serial
    constraint group_permissions_permissions_null_fk
    references permissions
);

create table if not exists profile
(
    user_id    serial
    constraint profile_user_id_uniqe
    unique
    constraint profile_auth_users_null_fk
    references auth_users
    on update cascade on delete cascade,
    first_name text,
    last_name  text,
    phone      text,
    created_at timestamp not null,
    updated_at timestamp not null,
    deleted_at timestamp
);

create table if not exists profile_groups
(
    user_id  serial
    constraint profile_groups_profile_null_fk
    references profile (user_id),
    group_id serial
    constraint profile_groups_groups_null_fk
    references groups
    );

create table if not exists profile_permissions
(
    user_id serial
    constraint profile_permissions_profile_null_fk
    references profile (user_id),
    perm_id serial
    constraint profile_permissions_permissions_null_fk
    references permissions
    );

