package models

import "time"

type Profile struct {
	UserId      int64         `json:"user_id" db:"user_id"`
	FirstName   *string       `json:"first_name" db:"first_name" validate:"required,lte=30"`
	LastName    *string       `json:"last_name" db:"last_name" validate:"required,lte=30"`
	Phone       *string       `json:"phone" db:"phone" validate:"required"`
	CreatedAt   time.Time     `json:"created_at,omitempty" db:"created_at"`
	UpdatedAt   time.Time     `json:"updated_at,omitempty" db:"updated_at"`
	DeletedAt   *time.Time    `json:"deleted_at" db:"deleted_at"`
	Groups      []*Group      `json:"groups"`
	Permissions []*Permission `json:"permissions"`
}
