package models

import "time"

type Group struct {
	GroupId     int64         `json:"group_id" db:"group_id"`
	Name        string        `json:"name" db:"name"`
	Permissions []*Permission `json:"permissions"`
	CreatedAt   time.Time     `json:"created_at,omitempty" db:"created_at"`
	UpdatedAt   time.Time     `json:"updated_at,omitempty" db:"updated_at"`
	DeletedAt   *time.Time    `json:"deleted_at" db:"deleted_at"`
}
