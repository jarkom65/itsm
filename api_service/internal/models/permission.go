package models

import "time"

type Permission struct {
	PermId    int64      `json:"perm_id" db:"perm_id"`
	Name      string     `json:"name" db:"name"`
	CreatedAt time.Time  `json:"created_at,omitempty" db:"created_at" `
	UpdatedAt time.Time  `json:"updated_at,omitempty" db:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at" db:"deleted_at"`
}
