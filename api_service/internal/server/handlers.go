package server

import (
	profileHttp "git.jetbrains.space/extcloud/extitsm/api_service/internal/profile/delivery/http"
	profileRepository "git.jetbrains.space/extcloud/extitsm/api_service/internal/profile/repository"
	profileUseCase "git.jetbrains.space/extcloud/extitsm/api_service/internal/profile/usecase"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"go.opentelemetry.io/otel"
	"net/http"
	"strings"
)

func (s *Server) Handlers(e *echo.Echo) error {
	otel.SetTracerProvider(s.tracer)

	profileRepo := profileRepository.NewProfileRepo(s.db)
	profileUC := profileUseCase.NewProfileUseCase(s.cfg, profileRepo, s.logger)

	profileHandlers := profileHttp.NewProfileHandlers(s.cfg, profileUC, s.logger, s.tracer)

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, echo.HeaderXRequestID},
	}))
	e.Use(middleware.RecoverWithConfig(middleware.RecoverConfig{
		StackSize:         1 << 10, // 1 KB
		DisablePrintStack: true,
		DisableStackAll:   true,
	}))
	e.Use(middleware.RequestID())
	e.Use(middleware.GzipWithConfig(middleware.GzipConfig{
		Level: 5,
		Skipper: func(c echo.Context) bool {
			return strings.Contains(c.Request().URL.Path, "swagger")
		},
	}))
	e.Use(middleware.Secure())
	e.Use(middleware.BodyLimit("2M"))
	v1 := e.Group("/api/v1")

	health := v1.Group("/health")
	profile := v1.Group("/profile")

	profileHttp.ProfileRoutes(profile, profileHandlers)

	health.GET("", func(c echo.Context) error {
		tr := s.tracer.Tracer("HTTP - HealthCheck")
		_, span := tr.Start(c.Request().Context(), "HTTP - HealthCheck")
		defer span.End()
		s.logger.Info("Health check RequestID: %s")
		return c.JSON(http.StatusOK, map[string]string{"status": "OK"})
	})
	return nil
}
