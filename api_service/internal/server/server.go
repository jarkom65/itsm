package server

import (
	"context"
	"git.jetbrains.space/extcloud/extitsm/api_service/config"
	"git.jetbrains.space/extcloud/extitsm/api_service/pkg/logger"
	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo/v4"
	"go.opentelemetry.io/otel/sdk/trace"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

const (
	maxHeaderBytes = 1 << 20
	ctxTimeout     = 5
)

type Server struct {
	echo   *echo.Echo
	cfg    *config.Config
	db     *sqlx.DB
	logger *logger.Logger
	tracer *trace.TracerProvider
}

func NewServer(cfg *config.Config, db *sqlx.DB, logger *logger.Logger, tracer *trace.TracerProvider) *Server {
	return &Server{echo: echo.New(), cfg: cfg, db: db, logger: logger, tracer: tracer}
}

func (s *Server) Run() error {
	server := &http.Server{
		Addr:           s.cfg.Http.Port,
		ReadTimeout:    time.Second * s.cfg.Http.ReadTimeout,
		WriteTimeout:   time.Second * s.cfg.Http.WriteTimeout,
		MaxHeaderBytes: maxHeaderBytes,
	}
	go func() {
		s.logger.Info("Server is listening on PORT: %s", s.cfg.Http.Port)
		if err := s.echo.StartServer(server); err != nil {
			s.logger.Fatal("Error starting Server: ", err)
		}
	}()

	if err := s.Handlers(s.echo); err != nil {
		return err
	}

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)

	<-quit

	ctx, shutdown := context.WithTimeout(context.Background(), ctxTimeout*time.Second)
	defer shutdown()

	s.logger.Info("Server Exited Properly")
	return s.echo.Server.Shutdown(ctx)

}
