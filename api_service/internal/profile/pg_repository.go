package profile

import (
	"context"
	"git.jetbrains.space/extcloud/extitsm/api_service/internal/models"
)

type Repository interface {
	GetById(ctx context.Context, id int) (*models.Profile, error)
	GetIdGroups(ctx context.Context, id int64) ([]*models.Group, error)
}
