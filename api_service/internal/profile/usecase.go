package profile

import (
	"context"
	"git.jetbrains.space/extcloud/extitsm/api_service/internal/models"
)

type UseCase interface {
	GetById(ctx context.Context, id int) (*models.Profile, error)
}
