package http

import (
	"git.jetbrains.space/extcloud/extitsm/api_service/internal/profile"
	"github.com/labstack/echo/v4"
)

func ProfileRoutes(profileGroup *echo.Group, h profile.Handlers) {
	profileGroup.GET("", h.Get())
}
