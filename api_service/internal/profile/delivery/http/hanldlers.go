package http

import (
	"git.jetbrains.space/extcloud/extitsm/api_service/config"
	"git.jetbrains.space/extcloud/extitsm/api_service/internal/profile"
	"git.jetbrains.space/extcloud/extitsm/api_service/pkg/http_errors"
	"git.jetbrains.space/extcloud/extitsm/api_service/pkg/logger"
	"github.com/labstack/echo/v4"
	"go.opentelemetry.io/otel/sdk/trace"
	"net/http"
)

type profileHandlers struct {
	cfg       *config.Config
	profileUC profile.UseCase
	logger    *logger.Logger
	tracer    *trace.TracerProvider
}

func NewProfileHandlers(cfg *config.Config, profileUC profile.UseCase, logger *logger.Logger, tracer *trace.TracerProvider) profile.Handlers {
	return &profileHandlers{cfg: cfg, profileUC: profileUC, logger: logger, tracer: tracer}
}

func (p *profileHandlers) Get() echo.HandlerFunc {
	return func(c echo.Context) error {
		tr := p.tracer.Tracer("ProfileHandler - Get")
		_, span := tr.Start(c.Request().Context(), "ProfileHandler - Get")
		defer span.End()
		prf, err := p.profileUC.GetById(c.Request().Context(), 18)
		if err != nil {
			return http_errors.ErrorCtxResponse(c, err)
		}
		return c.JSON(http.StatusOK, prf)
	}
}
