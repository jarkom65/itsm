package repository

import (
	"context"
	"errors"
	"git.jetbrains.space/extcloud/extitsm/api_service/internal/models"
	"git.jetbrains.space/extcloud/extitsm/api_service/internal/profile"
	"github.com/jmoiron/sqlx"
)

type profileRepo struct {
	db *sqlx.DB
}

func NewProfileRepo(db *sqlx.DB) profile.Repository {
	return &profileRepo{db: db}
}

func (r *profileRepo) GetById(ctx context.Context, id int) (*models.Profile, error) {
	prf := &models.Profile{}
	if err := r.db.GetContext(ctx, prf, getProfileByID, id); err != nil {
		return nil, errors.New("profileRepo - GetById - error: " + err.Error())
	}
	return prf, nil
}

func (r *profileRepo) GetIdGroups(ctx context.Context, id int64) ([]*models.Group, error) {
	grps := []*models.Group{}
	var ids []int
	if err := r.db.Select(&ids, searchProfileGroup, id); err != nil {
		return nil, errors.New("profileRepo - GetIdGroups - error: " + err.Error())
	}
	for _, i := range ids {
		grp := &models.Group{}
		err := r.db.GetContext(ctx, grp, getGroupByID, i)
		if err != nil {
			return nil, errors.New("profileRepo - GetIdGroups - error: " + err.Error())
		}
		grps = append(grps, grp)
	}
	return grps, nil
}
