package repository

const (
	getProfileByID     = `select user_id, first_name, last_name, phone, created_at, updated_at, deleted_at FROM profile WHERE user_id = $1`
	searchProfileGroup = `SELECT group_id FROM profile_groups WHERE user_id = $1`
	getGroupByID       = `SELECT * FROM groups WHERE group_id = $1`
)
