package usecase

import (
	"context"
	"errors"
	"git.jetbrains.space/extcloud/extitsm/api_service/config"
	"git.jetbrains.space/extcloud/extitsm/api_service/internal/models"
	"git.jetbrains.space/extcloud/extitsm/api_service/internal/profile"
	"git.jetbrains.space/extcloud/extitsm/api_service/pkg/logger"
)

type profileUC struct {
	cfg         *config.Config
	profileRepo profile.Repository
	logger      *logger.Logger
}

func NewProfileUseCase(cfg *config.Config, profileRepo profile.Repository, logger *logger.Logger) profile.UseCase {
	return &profileUC{cfg, profileRepo, logger}
}

func (uc *profileUC) GetById(ctx context.Context, id int) (*models.Profile, error) {
	prf, err := uc.profileRepo.GetById(ctx, id)
	if err != nil {
		return nil, errors.New("profileUC - GetById - error: " + err.Error())
	}
	groups, err := uc.profileRepo.GetIdGroups(ctx, prf.UserId)
	prf.Groups = append(prf.Groups, groups...)
	return prf, nil
}
