package main

import (
	"git.jetbrains.space/extcloud/extitsm/api_service/config"
	"git.jetbrains.space/extcloud/extitsm/api_service/internal/server"
	"git.jetbrains.space/extcloud/extitsm/api_service/pkg/jaeger"
	"git.jetbrains.space/extcloud/extitsm/api_service/pkg/logger"
	"git.jetbrains.space/extcloud/extitsm/api_service/pkg/postgres"
	"log"
)

func main() {
	log.Println("Starting api server")
	cfg, err := config.ParseConfig()
	if err != nil {
		log.Fatal(err)
	}
	appLogger := logger.New("debug")
	appLogger.Info("Starting API server")

	psqlDB, err := postgres.NewDB(cfg)
	if err != nil {
		appLogger.Fatal("Postgresql init: %s", err)
	} else {
		appLogger.Info("Postgres connected, Status: %#v", psqlDB.Stats())
	}
	defer psqlDB.Close()

	tp, err := jaeger.TracerProvider(cfg.Jaeger.URL, cfg.Jaeger.Service, cfg.Jaeger.Environment, cfg.Jaeger.ID)
	if err != nil {
		appLogger.Fatal("Jaeger not connected, Status: %#v", err)
	}

	s := server.NewServer(cfg, psqlDB, appLogger, tp)
	if err = s.Run(); err != nil {
		appLogger.Fatal(err)
	}

}
