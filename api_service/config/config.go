package config

import (
	"log"
	"os"
	"time"

	"github.com/spf13/viper"
)

const (
	JAEGER_URL = "JAEGER_URL"
	HTTP_PORT  = "HTTP_PORT"
)

// Config of application
type Config struct {
	AppVersion string
	Logger     Logger
	Http       Http
	Postgres   Postgres
	JWT        JWT
	Jaeger     Jaeger
}

type JWT struct {
	SignedKey   string
	TokeExpires time.Duration
}

type Http struct {
	Port              string
	PprofPort         string
	Timeout           time.Duration
	ReadTimeout       time.Duration
	WriteTimeout      time.Duration
	CookieLifeTime    int
	SessionCookieName string
}

// Logger config
type Logger struct {
	DisableCaller     bool
	DisableStacktrace bool
	Encoding          string
	Level             string
}

type Postgres struct {
	Host     string
	Port     string
	User     string
	Password string
	Dbname   string
	SSLMode  bool
	PgDriver string
}

type Jaeger struct {
	URL         string
	Service     string
	Environment string
	ID          int64
}

func exportConfig() error {
	viper.SetConfigType("yaml")
	viper.AddConfigPath("./config")
	viper.SetConfigName("config.yaml")

	if err := viper.ReadInConfig(); err != nil {
		return err
	}
	return nil
}

// ParseConfig Parse config file
func ParseConfig() (*Config, error) {
	if err := exportConfig(); err != nil {
		return nil, err
	}

	var c Config
	err := viper.Unmarshal(&c)
	if err != nil {
		log.Printf("unable to decode into struct, %v", err)
		return nil, err
	}

	httpPort := os.Getenv(HTTP_PORT)
	if httpPort != "" {
		c.Http.Port = httpPort
	}

	urlJaeger := os.Getenv(JAEGER_URL)
	if httpPort != "" {
		c.Jaeger.URL = urlJaeger
	}

	return &c, nil
}
