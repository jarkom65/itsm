package main

import (
	"git.jetbrains.space/extcloud/extitsm/auth_service/config"
	"git.jetbrains.space/extcloud/extitsm/auth_service/internal/server"
	"git.jetbrains.space/extcloud/extitsm/auth_service/pkg/logger"
	"git.jetbrains.space/extcloud/extitsm/auth_service/pkg/postgres"
	"log"
)

func main() {
	log.Println("Starting products microservice")
	cfg, err := config.ParseConfig()
	if err != nil {
		log.Fatal(err)
	}
	appLogger := logger.New("debug")
	appLogger.Info("Starting user server")

	psqlDB, err := postgres.NewDB(cfg)
	if err != nil {
		appLogger.Fatal("Postgresql init: %s", err)
	} else {
		appLogger.Info("Postgres connected, Status: %#v", psqlDB.Stats())
	}
	defer psqlDB.Close()
	s := server.NewServer(appLogger, cfg, psqlDB)
	appLogger.Fatal(s.Run())

}
