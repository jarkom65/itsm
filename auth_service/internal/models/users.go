package models

import (
	"time"
)

type User struct {
	UserID    int        `json:"user_id" bson:"_id,omitempty" db:"user_id"`
	UserName  string     `json:"username" bson:"username,omitempty" validate:"required,min=4,max=250" db:"username"`
	Password  string     `json:"password"  bson:"password,omitempty" validate:"required,min=6,max=250" db:"password"`
	Email     string     `json:"email" bson:"email,omitempty" validate:"required,min=5,max=250" db:"email"`
	CreatedAt time.Time  `json:"created_at" bson:"createdAt,omitempty" db:"created_at"`
	UpdatedAt time.Time  `json:"updated_at" bson:"updatedAt,omitempty"  db:"updated_at"`
	DeletedAT *time.Time `json:"deleted_at" bson:"deletedAt" db:"deleted_at"`
}

type Token struct {
	UserId       int    `json:"user_id" db:"user_id"`
	AccessToken  string `json:"access_token" db:"access_token"`
	RefreshToken string `json:"refresh_token" db:"refresh_token"`
}

type UsersList struct {
	TotalCount int64   `json:"total_count"`
	TotalPages int64   `json:"total_pages"`
	Page       int64   `json:"page"`
	Size       int64   `json:"size"`
	HasMore    bool    `json:"has_more"`
	Users      []*User `json:"users"`
}
