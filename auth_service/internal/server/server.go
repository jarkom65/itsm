package server

import (
	"context"
	"git.jetbrains.space/extcloud/extitsm/auth_service/config"
	authHTPPv1 "git.jetbrains.space/extcloud/extitsm/auth_service/internal/auth/delivery/http/v1"
	"git.jetbrains.space/extcloud/extitsm/auth_service/internal/auth/repository"
	"git.jetbrains.space/extcloud/extitsm/auth_service/internal/auth/usecase"
	"git.jetbrains.space/extcloud/extitsm/auth_service/internal/middlewares"
	"git.jetbrains.space/extcloud/extitsm/auth_service/pkg/logger"
	"git.jetbrains.space/extcloud/extitsm/auth_service/pkg/utils"
	"github.com/go-playground/validator/v10"
	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"os"
	"os/signal"
	"syscall"
)

const (
	maxHeaderBytes  = 1 << 20
	gzipLevel       = 5
	stackSize       = 1 << 10 // 1 KB
	csrfTokenHeader = "X-CSRF-Token"
	bodyLimit       = "2M"
)

type server struct {
	log  *logger.Logger
	cfg  *config.Config
	db   *sqlx.DB
	echo *echo.Echo
}

func NewServer(log *logger.Logger, cfg *config.Config, db *sqlx.DB) *server {
	return &server{log: log, cfg: cfg, db: db, echo: echo.New()}
}

func (s *server) Run() error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	validate := validator.New()
	hasher := utils.NewHasher(s.cfg)
	authDB := repository.NewAuthRepository(s.db)
	jwtManger, err := utils.NewJWTManager(s.cfg)
	if err != nil {
		s.log.Fatal("server- Run - error : %v", err)
		return err
	}
	authUC := usecase.NewAuthUC(authDB, s.log, hasher, jwtManger)
	mw := middlewares.NewMiddlewareManager(s.log, s.cfg)
	v1 := s.echo.Group("/api/v1")
	v1.Use(mw.Metrics)

	authHandlers := authHTPPv1.NewAuthHandlers(s.log, authUC, validate, v1.Group("/auth"), mw)
	authHandlers.MapRoutes()
	go func() {
		s.log.Info("Server is listening on PORT: %s", s.cfg.Http.Port)
		s.runHttpServer()
	}()
	metricsServer := echo.New()
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)
	select {
	case v := <-quit:
		s.log.Error("signal.Notify: %v", v)
	case done := <-ctx.Done():
		s.log.Error("ctx.Done: %v", done)
	}

	if err := s.echo.Server.Shutdown(ctx); err != nil {
		return errors.Wrap(err, "echo.Server.Shutdown")
	}

	if err := metricsServer.Shutdown(ctx); err != nil {
		s.log.Error("metricsServer.Shutdown: %v", err)
	}
	s.log.Info("Server Exited Properly")
	return nil
}
