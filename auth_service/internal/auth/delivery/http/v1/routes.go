package v1

func (a *authHandlers) MapRoutes() {
	a.group.POST("", a.CreateUsers())
	a.group.GET("", a.Login())
}
