package v1

import (
	"git.jetbrains.space/extcloud/extitsm/auth_service/internal/auth"
	"git.jetbrains.space/extcloud/extitsm/auth_service/internal/middlewares"
	"git.jetbrains.space/extcloud/extitsm/auth_service/internal/models"
	"git.jetbrains.space/extcloud/extitsm/auth_service/pkg/http_errors"
	"git.jetbrains.space/extcloud/extitsm/auth_service/pkg/logger"
	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"net/http"
)

type authHandlers struct {
	log      *logger.Logger
	authUC   auth.UseCase
	validate *validator.Validate
	group    *echo.Group
	mw       middlewares.MiddlewareManager
}

type UserINPUT struct {
	UserName string `json:"username"`
	Password string `json:"password"`
}

func NewAuthHandlers(log *logger.Logger, authUC auth.UseCase, validate *validator.Validate, group *echo.Group, mw middlewares.MiddlewareManager) *authHandlers {
	return &authHandlers{
		log:      log,
		authUC:   authUC,
		validate: validate,
		group:    group,
		mw:       mw,
	}
}

func (a *authHandlers) CreateUsers() echo.HandlerFunc {
	return func(c echo.Context) error {
		var user models.User
		if err := c.Bind(&user); err != nil {
			a.log.Error("authHandlers -CreateUsers- error: %v", err)
			return http_errors.ErrorCtxResponse(c, err)
		}
		if err := a.validate.StructCtx(c.Request().Context(), &user); err != nil {
			a.log.Error("validate.StructCtx: %v", err)
			return http_errors.ErrorCtxResponse(c, err)
		}
		usr, err := a.authUC.Create(c.Request().Context(), &user)
		if err != nil {
			a.log.Error("CreateUsers - echo.HandlerFunc - authUC.Create: %v", err.Error())
			return http_errors.ErrorCtxResponse(c, err)
		}
		return c.JSON(http.StatusCreated, usr)
	}
}

func (a *authHandlers) Login() echo.HandlerFunc {
	return func(c echo.Context) error {
		var user UserINPUT
		if err := c.Bind(&user); err != nil {
			a.log.Error("authHandlers -CreateUsers- error: %v", err)
			return http_errors.ErrorCtxResponse(c, err)
		}
		tokens, err := a.authUC.Login(c.Request().Context(), user.UserName, user.Password)
		if err != nil {
			a.log.Error("authHandlers - Login- error: %v", err)
			return http_errors.ErrorCtxResponse(c, err)
		}
		return c.JSON(http.StatusOK, tokens)
	}
}
