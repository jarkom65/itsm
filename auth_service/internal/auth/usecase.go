package auth

import (
	"context"
	"git.jetbrains.space/extcloud/extitsm/auth_service/internal/models"
)

type UseCase interface {
	Create(ctx context.Context, user *models.User) (*models.User, error)
	Login(ctx context.Context, username, password string) (*models.Token, error)
}
