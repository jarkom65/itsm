package auth

import (
	"context"
	"git.jetbrains.space/extcloud/extitsm/auth_service/internal/models"
)

type Repository interface {
	Register(ctx context.Context, user *models.User) (*models.User, error)
	GetByUsername(ctx context.Context, user *models.User) (*models.User, error)
	StoreToken(ctx context.Context, user *models.User, accessToken, refreshToken string) (*models.Token, error)
}
