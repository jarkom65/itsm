package auth

import "github.com/labstack/echo/v4"

type HttpDelivery interface {
	CreateUsers() echo.HandlerFunc
}
