package usecase

import (
	"context"
	errors "errors"
	"fmt"
	"git.jetbrains.space/extcloud/extitsm/auth_service/internal/auth"
	"git.jetbrains.space/extcloud/extitsm/auth_service/internal/models"
	"git.jetbrains.space/extcloud/extitsm/auth_service/pkg/logger"
	"git.jetbrains.space/extcloud/extitsm/auth_service/pkg/utils"
	"time"
)

type authUC struct {
	authRepo auth.Repository
	hasher   *utils.Hasher
	log      *logger.Logger
	jwt      *utils.Manager
}

func NewAuthUC(authRepo auth.Repository, log *logger.Logger, hasher *utils.Hasher, jwt *utils.Manager) *authUC {
	return &authUC{
		authRepo: authRepo,
		log:      log,
		hasher:   hasher,
		jwt:      jwt,
	}
}

func (a *authUC) Create(ctx context.Context, user *models.User) (*models.User, error) {
	hpassword, err := a.hasher.HashPassword(user.Password)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("authUC - Create error: %v", err))
	}
	user.Password = hpassword
	user.CreatedAt = time.Now().UTC()
	user.UpdatedAt = time.Now().UTC()
	return a.authRepo.Register(ctx, user)
}

func (a *authUC) Login(ctx context.Context, username, password string) (*models.Token, error) {
	user := &models.User{
		UserName: username,
	}
	user, err := a.authRepo.GetByUsername(ctx, user)
	if err != nil {
		return nil, errors.New("authUC - Login - error: " + err.Error())
	}
	checkPass := a.hasher.CheckPasswordHash(password, user.Password)
	if checkPass != true {
		return nil, errors.New("password incorrect")
	}
	accessToken, err := a.jwt.NewJWT(string(rune(user.UserID)))
	if err != nil {
		return nil, errors.New("authUC - NewJWT - error: " + err.Error())
	}
	refreshToken, err := a.jwt.NewRefreshToken()
	if err != nil {
		return nil, errors.New("authUC - NewRefreshToken - error: " + err.Error())
	}
	return a.authRepo.StoreToken(ctx, user, accessToken, refreshToken)

}
