package repository

import (
	"context"
	"errors"
	"fmt"
	"git.jetbrains.space/extcloud/extitsm/auth_service/internal/auth"
	"git.jetbrains.space/extcloud/extitsm/auth_service/internal/models"
	"github.com/jmoiron/sqlx"
)

type authRepo struct {
	db *sqlx.DB
}

func NewAuthRepository(db *sqlx.DB) auth.Repository {
	return &authRepo{db: db}
}

func (r *authRepo) Register(ctx context.Context, user *models.User) (*models.User, error) {
	u := &models.User{}
	if err := r.db.QueryRowxContext(ctx, createUserQuery, &user.UserName, &user.Email, &user.Password, &user.CreatedAt, &user.UpdatedAt).StructScan(u); err != nil {
		return nil, errors.New(fmt.Sprintf("authRepo - Register - error: %v", err.Error()))
	}
	return u, nil

}

func (r *authRepo) GetByUsername(ctx context.Context, user *models.User) (*models.User, error) {
	u := &models.User{}
	if err := r.db.QueryRowxContext(ctx, getUserQuery, &user.UserName).StructScan(u); err != nil {
		return nil, errors.New(fmt.Sprintf("authRepo - GetByUsername -error: %v", err.Error()))
	}
	return u, nil
}

func (r *authRepo) StoreToken(ctx context.Context, user *models.User, accessToken, refreshToken string) (*models.Token, error) {
	t := &models.Token{}
	if err := r.db.QueryRowxContext(ctx, insertTokenQuery, &user.UserID, accessToken, refreshToken).StructScan(t); err != nil {
		return nil, errors.New(fmt.Sprintf("authRepo - Tokens -error: %v", err.Error()))
	}
	return t, nil
}
