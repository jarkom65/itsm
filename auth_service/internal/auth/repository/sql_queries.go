package repository

const (
	createUserQuery = `INSERT INTO auth_users (username, email, password, created_at, updated_at) VALUES ($1, $2, $3, $4, $5) RETURNING *`

	getUserQuery = `SELECT * FROM auth_users WHERE username = $1`

	insertTokenQuery = `INSERT INTO auth_tokens (user_id, access_token, refresh_token) VALUES ($1, $2, $3) ON CONFLICT(user_id) DO UPDATE SET access_token = $2, refresh_token= $3 RETURNING *`
)
