package utils

import (
	"errors"
	"fmt"
	"git.jetbrains.space/extcloud/extitsm/auth_service/config"
	"github.com/golang-jwt/jwt/v4"
	"math/rand"
	"time"
)

type TokenManager interface {
	NewJWT(userId string, ttl time.Duration) (string, error)
	Parse(accessToken string) (string, error)
	NewRefreshToken() (string, error)
}

type Manager struct {
	cfg *config.Config
}

func NewJWTManager(cfg *config.Config) (*Manager, error) {
	if cfg.JWT.SignedKey == "" {
		return nil, errors.New("empty signing key")
	}

	return &Manager{cfg: cfg}, nil
}

func (m *Manager) NewJWT(userId string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.RegisteredClaims{
		ExpiresAt: jwt.NewNumericDate(time.Now().Add(m.cfg.JWT.TokeExpires * time.Hour)),
		IssuedAt:  jwt.NewNumericDate(time.Now()),
		Subject:   userId,
	})

	return token.SignedString([]byte(m.cfg.JWT.SignedKey))
}

func (m *Manager) Parse(accessToken string) (string, error) {
	token, err := jwt.Parse(accessToken, func(token *jwt.Token) (i interface{}, err error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(m.cfg.JWT.SignedKey), nil
	})
	if err != nil {
		return "", err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return "", fmt.Errorf("error get user claims from token")
	}

	return claims["sub"].(string), nil
}

func (m *Manager) NewRefreshToken() (string, error) {
	b := make([]byte, 32)

	s := rand.NewSource(time.Now().Unix())
	r := rand.New(s)

	if _, err := r.Read(b); err != nil {
		return "", err
	}

	return fmt.Sprintf("%x", b), nil
}
