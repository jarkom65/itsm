package utils

import (
	"git.jetbrains.space/extcloud/extitsm/auth_service/config"
	"golang.org/x/crypto/bcrypt"
)

type Hasher struct {
	cfg *config.Config
}

func NewHasher(cfg *config.Config) *Hasher {
	return &Hasher{cfg}
}

func (h *Hasher) HashPassword(password string) (string, error) {
	password = password + h.cfg.Hasher.Salt
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), h.cfg.Hasher.Cost)
	return string(bytes), err
}

func (h *Hasher) CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password+h.cfg.Hasher.Salt))
	return err == nil
}
