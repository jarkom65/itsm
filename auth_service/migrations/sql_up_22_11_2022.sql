create table if not exists auth_users
(
    user_id    serial
    constraint auth_users_pk_user_id
    primary key,
    username   text      not null
    constraint auth_users_uniq_username
    unique,
    email      text      not null
    constraint auth_users_pk_uniq_email
    unique,
    created_at timestamp not null,
    updated_at timestamp not null,
    deleted_at timestamp,
    password   text      not null
);

create table if not exists auth_tokens
(
    user_id       integer not null
    constraint auth_tokens_uniq_user_id
    unique
    constraint auth_tokens_auth_users_null_fk
    references auth_users
    on update cascade on delete cascade,
    access_token  text    not null
    constraint auth_tokens_pk_uniq_access_token
    unique,
    refresh_token text    not null
    constraint auth_tokens_pk_uniq_refresh_token
    unique
);

